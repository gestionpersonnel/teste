package Modelo;
public class Corrente{
	private float taxa;
	private int id;
	private int id_conta;
	public int getId_conta(){
	    return id_conta;
	}
	public void setId_conta(int id_conta){
	    this.id_conta = id_conta;
	} 
	public float getTaxa() {
		return taxa;
	}
	public void setTaxa(float taxa) {
		this.taxa = taxa;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
}
