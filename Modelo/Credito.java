package Modelo;
public class Credito {
	private String prazo;
	private double limite;
	private int id;
	private int id_cartao;
	private int id_usuario;
	public int getId_cartao() {
		return id_cartao;
	}
	public void setId_cartao(int id_cartao) {
		this.id_cartao = id_cartao;
	}
	public int getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}
	public String getPrazo() {
		return prazo;
	}
	public void setPrazo(String prazo) {
		this.prazo = prazo;
	}
	public double getLimite() {
		return limite;
	}
	public void setLimite(double limite) {
		this.limite = limite;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}