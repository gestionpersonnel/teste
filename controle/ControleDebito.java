package controle;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import Modelo.Debito;
public class ControleDebito {
	public boolean inserir(Debito debito) throws SQLException {
		boolean resultado = false;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("INSERT INTO debito(id_cartao,id_usuario) VALUES(?,?);");		
		ps.setInt(1, debito.getId_cartao());
		ps.setInt(2, debito.getId_usuario());
		if(!ps.execute()) {
			resultado = true;
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
		return resultado;
	}

	public boolean deletar(int id) throws SQLException{
		boolean resultado= false;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("DELETE FROM Debito WHERE id=?;");
		ps.setInt(1, id);
		if(!ps.execute()){
			resultado = true;
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
				
		return resultado;
	}
	
	public Debito consultar(int id_usuario) throws SQLException{
		Debito debito = null;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("SELECT * FROM Debito WHERE id_usuario=?;");
		ps.setInt(1,id_usuario);
		ResultSet rs = ps.executeQuery();
		if(rs != null && rs.next()){
			Debito deb = new Debito();
			deb.setId(rs.getInt("id"));
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}				
		return debito;
	}	
}
