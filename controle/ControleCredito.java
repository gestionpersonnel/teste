package controle;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import Modelo.Credito;
public class ControleCredito {
	public boolean inserir(Credito credito) throws SQLException {
		boolean resultado = false;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("INSERT INTO Credito(prazo,limite,id_cartao,id_usuario) VALUES(?,?,?,?);");
		ps.setString(1, credito.getPrazo());		
		ps.setDouble(2, credito.getLimite());
		ps.setInt(3, credito.getId_cartao());
		ps.setInt(4, credito.getId_usuario());
		if(!ps.execute()) {
			resultado = true;
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
		return resultado;
	}
	public boolean atualizar(Credito credito) throws SQLException {
		boolean resultado = false;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("UPDATE Credito SET prazo=?, limite=? WHERE id=?");
		ps.setString(1, credito.getPrazo());		
		ps.setDouble(2, credito.getLimite());		
		if(!ps.execute()){
			resultado = true;
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
		return resultado;
	}
	public boolean deletar(int id) throws SQLException{
		boolean resultado= false;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("DELETE FROM Credito WHERE id=?;");
		ps.setInt(1, id);
		if(!ps.execute()){
			resultado = true;
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
				
		return resultado;
	}
	public Credito consultar(int id_usuario) throws SQLException{
		Credito credito = null;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("SELECT * FROM Credito WHERE id_usuario=?;");
		ps.setInt(1,id_usuario);
		ResultSet rs = ps.executeQuery();
		if(rs != null && rs.next()){
			credito = new Credito();
			credito.setPrazo(rs.getString("prazo"));	
			credito.setLimite(rs.getFloat("limite"));				
			credito.setId(rs.getInt("id"));
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
				
		return credito;

	}
	public ArrayList<Credito> consultarTodos() throws SQLException{
		ArrayList<Credito> lista =null;		
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("SELECT * FROM Credito;");
		ResultSet rs = ps.executeQuery();
		if(rs != null){
			lista = new ArrayList<Credito>();
			while(rs.next()) {
				Credito credito = new Credito();
				credito.setId(rs.getInt("id"));
				credito.setLimite(rs.getFloat("limite"));
				credito.setPrazo(rs.getString("prazo"));
				lista.add(credito);
			}
		}
		new Conexao().fecharConexao(con);
		return lista;

	}
}
