package controle;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import Modelo.compra;
public class ControleCompra {
	public boolean inserir(compra com){
		boolean resultado = false;
		Connection con = new Conexao().abrirConexao();
		try {
			PreparedStatement ps = con.prepareStatement("INSERT INTO compra(produto,cartao,valor) VALUES(?,?,?);");
			ps.setString(1, com.getProduto());
			ps.setString(2, com.getCartao());
			ps.setFloat(3, com.getValor());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	public boolean atualizar(compra com) throws SQLException {
		boolean resultado = false;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("UPDATE Usuario SET produto=?, cartao=?, valor=? WHERE id=?");
		ps.setString(1, com.getProduto());
		ps.setString(2, com.getCartao());
		ps.setFloat(3, com.getValor());
		ps.setInt(4, com.getId());
		if(!ps.execute()){
			resultado = true;
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
		return resultado;
	}
	public boolean deletar(int id) throws SQLException{
		boolean resultado= false;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("DELETE FROM compra WHERE id=?;");
		ps.setInt(1, id);
		if(!ps.execute()){
			resultado = true;
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
				
		return resultado;
	}
	public compra consultar(int id) throws SQLException{
		compra com = null;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("SELECT * FROM compra WHERE id=?;");
		ps.setInt(1,id);
		ResultSet rs = ps.executeQuery();
		if(rs != null && rs.next()){
			com = new compra();
			com.setId(rs.getInt("id"));
			com.setProduto(rs.getString("produto"));
			com.setCartao(rs.getString("cartao"));
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
				
		return com;

	}
	public ArrayList<compra> consultarTodos() throws SQLException{
		ArrayList<compra> lista = null;		
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("SELECT * FROM compra;");
		ResultSet rs = ps.executeQuery();
		if(rs != null){
			lista = new ArrayList<compra>();
			while(rs.next()) {
				compra com = new compra();
				com.setProduto(rs.getString("produto"));
				com.setCartao(rs.getString("cartao"));
				com.setValor(rs.getFloat("valor"));
				com.setId(rs.getInt("id"));
				lista.add(com);
			}
		}
		new Conexao().fecharConexao(con);
		return lista;
	}
}
