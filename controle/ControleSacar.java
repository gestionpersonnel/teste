package controle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import Modelo.sacar;
public class ControleSacar {
	public boolean inserir(sacar sac){
		boolean resultado = false;
		Connection con = new Conexao().abrirConexao();
		try {
			PreparedStatement ps = con.prepareStatement("INSERT INTO sacar(valor,data,id_usuario) VALUES(?,?,?);");
			ps.setFloat(1, sac.getValor());
			ps.setString(2, sac.getData());
			ps.setInt(3, sac.getId_usuario());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	public boolean deletar(int id) throws SQLException{
		boolean resultado= false;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("DELETE FROM sacar WHERE id=?;");
		ps.setInt(1, id);
		if(!ps.execute()){
			resultado = true;
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
				
		return resultado;
	}
	public ArrayList<sacar> consultarTodos() throws SQLException{
		ArrayList<sacar> lista = null;		
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("SELECT * FROM sacar;");
		ResultSet rs = ps.executeQuery();
		if(rs != null){
			lista = new ArrayList<sacar>();
			while(rs.next()) {
				sacar sac = new sacar();
				sac.setValor(rs.getFloat("valor"));
				sac.setData(rs.getString("data"));
				sac.setId(rs.getInt("id"));
				lista.add(sac);
			}
		}
		new Conexao().fecharConexao(con);
		return lista;
	}
}
