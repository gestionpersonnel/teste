package controle;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import Modelo.Usuario;
import Modelo.cartao;
import Modelo.conta;
public class ControleUsuario {
	public Usuario logar(Usuario user) throws SQLException{
		Usuario useer = null;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("SELECT * FROM usuario WHERE nome=? and senha=?");
		ps.setString(1, user.getNome());
		ps.setString(2, user.getSenha());
		ResultSet rs = ps.executeQuery();
		if(rs.next() && rs != null){
			useer = new Usuario();
			useer.setId(rs.getInt("id"));
			useer.setNome(rs.getString("nome"));
			useer.setSenha(rs.getString("senha"));
		}else{
			useer=null;
		}
		new Conexao().fecharConexao(con);	
		return useer;
	}


	public boolean inserir(Usuario user){
		boolean resultado = false;
		Connection con = new Conexao().abrirConexao();
		try {
			PreparedStatement ps = con.prepareStatement("INSERT INTO Usuario(nome,senha,email) VALUES(?,?,?);");
			ps.setString(1, user.getNome());
			ps.setString(2, user.getSenha());
			ps.setString(3, user.getEmail());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	public boolean atualizar(Usuario user) throws SQLException {
		boolean resultado = false;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("UPDATE Usuario SET nome=?, email=?, senha=? WHERE id=?");
		ps.setString(1, user.getNome());
		ps.setString(2, user.getEmail());
		ps.setString(3, user.getSenha());
		ps.setInt(4, user.getId());
		if(!ps.execute()){
			resultado = true;
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
		return resultado;
	}
	public boolean deletar(int id) throws SQLException{
		boolean resultado= false;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("DELETE FROM Usuario WHERE id=?;");
		ps.setInt(1, id);
		if(!ps.execute()){
			resultado = true;
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
				
		return resultado;
	}
	public Usuario consultar(int id) throws SQLException{
		Usuario user = null;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("SELECT * FROM usuario WHERE id=?;");
		ps.setInt(1,id);
		ResultSet rs = ps.executeQuery();
		if(rs != null && rs.next()){
			user = new Usuario();
			user.setId(rs.getInt("id"));
			user.setNome(rs.getString("nome"));
			user.setSenha(rs.getString("senha"));
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
				
		return user;

	}
	public ArrayList<Usuario> consultarTodos() throws SQLException{
		ArrayList<Usuario> lista = null;	
		ArrayList<conta> lista1 = null;
		ArrayList<cartao> lista2 = null;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("SELECT usu.id,car.id, co.id FROM usuario usu INNER JOIN cartao car ON usu.id=car.id INNER JOIN conta co ON usu.id=co.id;");
		ResultSet rs = ps.executeQuery();
		if(rs != null){
			Usuario user = new Usuario();
			conta co = new conta();
			cartao car = new cartao();
			lista = new ArrayList<Usuario>();
			lista1 = new ArrayList<conta>();
			lista2 = new ArrayList<cartao>();
			while(rs.next()) {
				lista.add(user);
				lista1.add(co);
				lista2.add(car);
			}
		}
		new Conexao().fecharConexao(con);
		return lista;

	}
	
}
