package controle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import Modelo.cartao;
public class ControleCartao {
	public boolean inserir(cartao car){
		boolean resultado = false;
		Connection con = new Conexao().abrirConexao();
		try {
			PreparedStatement ps = con.prepareStatement("INSERT INTO cartao(nome,id_usuario) VALUES(?,?);");			
			ps.setString(1, car.getNome());			
			ps.setInt(2, car.getId_usuario());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}

	public boolean atualizar(cartao car) throws SQLException {
		boolean resultado = false;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("UPDATE cartao SET nome=? WHERE id=?");
		ps.setString(2, car.getNome());
		ps.setInt(3, car.getId());
		if(!ps.execute()){
			resultado = true;
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
		return resultado;
	}
	public boolean deletar(int id) throws SQLException{
		boolean resultado= false;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("DELETE FROM conta WHERE id=?;");
		ps.setInt(1, id);
		if(!ps.execute()){
			resultado = true;
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
				
		return resultado;
	}
	public cartao consultar(int id_usuario) throws SQLException{
		cartao car = null;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("SELECT * FROM usuario WHERE id_usuario=?;");
		ps.setInt(1,id_usuario);
		ResultSet rs = ps.executeQuery();
		if(rs != null && rs.next()){
			car = new cartao();
			car.setId(rs.getInt("id"));
			car.setNome(rs.getString("nome"));
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
				
		return car;

	}
	public ArrayList<cartao> consultarTodos() throws SQLException{
		ArrayList<cartao> lista = null;		
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("SELECT * FROM cartao;");
		ResultSet rs = ps.executeQuery();
		if(rs != null){
			lista = new ArrayList<cartao>();
			while(rs.next()) {
				cartao car = new cartao();
				car.setNome(rs.getString("nome"));
				car.setId(rs.getInt("id"));
				lista.add(car);
			}
		}
		new Conexao().fecharConexao(con);
		return lista;
	}
	public cartao consultarId(int id_usuario) throws SQLException{
		cartao ca = null;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("SELECT id FROM cartao WHERE id_usuario=?;");
		ps.setInt(1,id_usuario);
		ResultSet rs = ps.executeQuery();
		if(rs != null && rs.next()){
			ca =new cartao();
			ca.setId(rs.getInt("id"));
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao consultar as informações.");
		}
				
		return ca;

	}

}
