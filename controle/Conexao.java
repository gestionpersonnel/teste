package controle;

import java.sql.Connection; //abrir e fechar a conexao
import java.sql.SQLException;
//import com.mysql.jdbc.Driver;
import java.sql.DriverManager;

public final class Conexao {
	public Connection abrirConexao() {
		Connection connect = null ;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String banco = "javaweb";
			String servidor = "jdbc:mysql://localhost/" + banco;
			String user = "root";
			String pwd ="";
			connect = DriverManager.getConnection(servidor, user, pwd);
		}catch(SQLException e ) {
			e.getMessage();
		}catch(Exception e) {
			e.getMessage();
		}
		return connect;	
	}
	public void fecharConexao(Connection con){
		try {
			con.close();
		}catch(Exception e) {
			e.getMessage();
		}
	}
}
