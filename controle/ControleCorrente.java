package controle;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import Modelo.Corrente;
public class ControleCorrente {
	public boolean inserir(Corrente corr) throws SQLException {
		boolean resultado = false;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("INSERT INTO Corrente(taxa) VALUES(?);");
		ps.setFloat(1, corr.getTaxa());
		if(!ps.execute()) {
			resultado = true;
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
		return resultado;
	}
	public boolean atualizar(Corrente cor) throws SQLException {
		boolean resultado = false;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("UPDATE Corrente SET taxa=? WHERE id=?");
		ps.setFloat(1, cor.getTaxa());
		if(!ps.execute()){
			resultado = true;
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
		return resultado;
	}
	public boolean deletar(int id) throws SQLException{
		boolean resultado= false;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("DELETE FROM Corrente WHERE id=?;");
		ps.setInt(1, id);
		if(!ps.execute()){
			resultado = true;
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
				
		return resultado;
	}
	public Corrente consultar(int id) throws SQLException{
		Corrente cor = null;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("SELECT * FROM Corrente WHERE id=?;");
		ps.setInt(1,id);
		ResultSet rs = ps.executeQuery();
		if(rs != null && rs.next()){
			cor = new Corrente();
			cor.setTaxa(rs.getFloat("taxa"));
			cor.setId(rs.getInt("id"));
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
				
		return cor;

	}
	public ArrayList<Corrente> consultarTodos() throws SQLException{
		ArrayList<Corrente> lista = null;		
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("SELECT * FROM Corrente;");
		ResultSet rs = ps.executeQuery();
		if(rs != null){
			lista = new ArrayList<Corrente>();
			while(rs.next()) {
				Corrente cor = new Corrente();
				cor.setId(rs.getInt("id"));
				cor.setTaxa(rs.getFloat("taxa"));
				lista.add(cor);
			}
		}
		new Conexao().fecharConexao(con);
		return lista;

	}
}
