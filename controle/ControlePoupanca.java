package controle;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import Modelo.Poupanca;
public class ControlePoupanca {
	public boolean inserir(Poupanca pou) throws SQLException {
		boolean resultado = false;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("INSERT INTO Poupanca(id_conta) VALUES(?);");
		ps.setInt(1, pou.getId_conta());
		if(!ps.execute()) {
			resultado = true;
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
		return resultado;
	}
	public boolean deletar(int id) throws SQLException{
		boolean resultado= false;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("DELETE FROM Poupanca WHERE id=?;");
		ps.setInt(1, id);
		if(!ps.execute()){
			resultado = true;
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
				
		return resultado;
	}
	public Poupanca consultar(int id) throws SQLException{
		Poupanca pou = null;
		Connection con = new Conexao().abrirConexao();
		PreparedStatement ps = con.prepareStatement("SELECT * FROM Poupanca WHERE id=?;");
		ps.setInt(1,id);
		ResultSet rs = ps.executeQuery();
		if(rs != null && rs.next()){
			pou = new Poupanca();
			pou.setId(rs.getInt("id"));
			new Conexao().fecharConexao(con);
		}else {
			throw new SQLException("Erro ao inserir as informações.");
		}
				
		return pou;

	}
}
