import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Modelo.sacar;
import controle.ControleSacar;
import Modelo.Usuario;
import Modelo.conta;
import controle.ControleConta;
@WebServlet("/recebendoSaque")
public class recebendoSaque extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ControleSacar con = new ControleSacar();
		sacar sac = new sacar();
		conta co = new conta();
		ControleConta cco = new ControleConta();
		sac.setData(request.getParameter("data"));
		sac.setValor(Float.parseFloat(request.getParameter("valor")));
		int id_usuario= co.getId_usuario();	
		try {
			Usuario user = (Usuario)request.getSession().getAttribute("usuario");
			sac.setId_usuario(user.getId());
			sac.setId_conta(cco.consultarId(user.getId()));
			float valorConta =cco.consultarValor(id_usuario, request);
			if(Float.parseFloat(request.getParameter("valor")) <= valorConta){					
				co.setValor(valorConta - Float.parseFloat(request.getParameter("valor")));
				cco.atualizarValor(co, request);	
				con.inserir(sac);
			//alert "Seu saldo agora �:co.getValor()"
			}else {
				//alert "Dados n�o compat�veis"
			}
			response.sendRedirect("saque.jsp");
		}catch(SQLException e){
			System.out.println(e.getMessage());		
			}
	}
}


