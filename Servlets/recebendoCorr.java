import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Modelo.Corrente;
import Modelo.Usuario;
import Modelo.conta;
import controle.ControleConta;
import controle.ControleCorrente;
@WebServlet("/recebendoCorr")
public class recebendoCorr extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		conta con = new conta();
		ControleConta ccon =new ControleConta();
		Corrente cor = new Corrente();
		conta co = new conta();
		ControleCorrente ccor = new ControleCorrente();
			cor.setTaxa(Float.parseFloat(request.getParameter("taxa")));
			con.setNumero(request.getParameter("numero"));
			con.setValor(Float.parseFloat(request.getParameter("valor")));
			Usuario user = (Usuario)request.getSession().getAttribute("usuario");
			co.setId_usuario(user.getId());
			if(ccon.inserir(con)) {
				try {
					int idConta = (ccon.consultarNumeroConta(co.getNumero()).getId());
					cor.setId_conta(idConta);
					if(ccor.inserir(cor)) {
						response.sendRedirect("index.jsp");
					}else {
						response.getWriter().print("conta nao criada");
					}
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}	
			}else {
				response.sendRedirect("cadastro.jsp");
			}
		}
}
