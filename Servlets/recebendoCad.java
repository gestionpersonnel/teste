import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Modelo.Usuario;
import controle.ControleUsuario;
@WebServlet("/recebendoCad")
public class recebendoCad extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			Usuario session = (Usuario)request.getSession().getAttribute("usuario");
			if(session == null) {
				ControleUsuario con = new ControleUsuario();
				Usuario user = new Usuario();
				user.setNome(request.getParameter("nome"));
				user.setSenha(request.getParameter("senha"));
				user.setEmail(request.getParameter("email"));
				if(con.inserir(user)) {
					Usuario userLog= con.logar(user);
					request.getSession().setAttribute("usuario", userLog);
					switch (request.getParameter("tipConta")) {
					case "cor":
						request.getRequestDispatcher("/corrente.jsp").forward(request, response);
						break;

					case "pou":
						request.getRequestDispatcher("/poupanca.jsp").forward(request, response);
						break;					
					}
				}
				
			}else {
				response.sendRedirect("index.jsp");
			}
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
