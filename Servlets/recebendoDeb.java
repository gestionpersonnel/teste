import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Modelo.Debito;
import controle.ControleDebito;
import Modelo.Usuario;
import Modelo.cartao;
import controle.ControleCartao;
@WebServlet("/recebendoDeb")
public class recebendoDeb extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Usuario user = (Usuario)request.getSession().getAttribute("usuario");
		if(user != null) {
			ControleCartao ccar = new ControleCartao();
			cartao car = new cartao();
			Debito deb = new Debito();
			ControleDebito cdeb = new ControleDebito();
			car.setNome(request.getParameter("nome"));
			car.setId_usuario(user.getId());
			if(ccar.inserir(car)) {
				try {
					int idCartao = ccar.consultarId(user.getId()).getId();
					deb.setId_cartao(idCartao);
					deb.setId_usuario(user.getId());
						if(cdeb.inserir(deb)){
							response.sendRedirect("debito.jsp");
						}else{
							response.getWriter().print("debito n inserido");
							}
				}catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}else {
				response.getWriter().print("cartao n inserido");
			}
	}
	}
}
