import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import controle.ControleCredito;
import Modelo.Credito;
import Modelo.Usuario;
import Modelo.cartao;
import controle.ControleCartao;
@WebServlet("/recebendoCred")
public class recebendoCred extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Usuario user = (Usuario)request.getSession().getAttribute("usuario");
		if(user != null) {
			ControleCredito ccred = new ControleCredito();
			Credito cred = new Credito();
			cartao car =new cartao();
			ControleCartao ccar= new ControleCartao();
			cred.setLimite(Double.parseDouble(request.getParameter("limite")));
			cred.setPrazo(request.getParameter("prazo"));
			car.setNome(request.getParameter("nome"));
			car.setId_usuario(user.getId());
			if(ccar.inserir(car)) {
				try {
					int idCartao = ccar.consultarId(user.getId()).getId();
					cred.setId_cartao(idCartao);
					cred.setId_usuario(user.getId());
						if(ccred.inserir(cred)) {
							response.sendRedirect("credito.jsp");
						}else{
							response.getWriter().print("credito n inserido");
							}
				}catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}else {
				response.getWriter().print("cartao n inserido");
			}
	}
	}
}



